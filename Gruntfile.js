var path = require('path');
var semver = require('semver');

module.exports = function(grunt) {

    var version = grunt.file.readJSON('package.json').version;

    var sUser = grunt.option('user');
    var sPwd = grunt.option('pwd');
    var sMode = grunt.option('mode');
    var sTransportKey = grunt.option('transport') || 'GW1K900046';

    var oHashMode = {
        prd: "prd",
        dev: "dev",
        test: "test"
    };

    if (!sMode || !(sMode in oHashMode)){
        sMode = oHashMode.dev;
    }

    if ((!sUser || !sPwd || !sTransportKey) && sMode === oHashMode.prd){
        grunt.fail.fatal("Usage: grunt -mode=prd -user=%USER_NAME% -pwd=%PASSWORD%. Credentials are used for the ui5 repository!");
    }

    var gruntData = {
        mode: sMode,
        server:{
            uri: "http://sapnw.cloud.local:8010",
            auth: {
                user: sUser,
                password: sPwd
            },
            repository:{
                package: 'Z_EXECUTION_BASE',
                bspApp: 'Z_BASE_UTIL',
                bspText: "Execution UI5 Library",
                transport: sTransportKey
            }
        },
        targetPath: "target",
        library: {
            name: "execution.base",
            version: semver.major(version) + "." + semver.minor(version),
            main: {
                path: "src",
                target: "target/resources/"
            },
            test: {
                path: "test",
                target: "target/test-resources/",
                testsuite: '/test-resources/execution/base/qunit/testsuite.qunit.html',
                reports: "surefire-reports"
            },
            docs: {
                path: 'docs',
                template: "jsdoc/template",
                config: "jsdoc/template/jsdoc.conf.json"
            }
        }
    }

    // Log time how long tasks take
    require('grunt-timer').init(grunt, {
        deferLogs: true,
        color: 'cyan'
    });

    grunt.loadTasks(path.join(process.cwd(), 'grunt/tasks'));

    require('load-grunt-config')(grunt, {

        configPath: path.join(process.cwd(), 'grunt/config'),

        // loads grunt plugins just-in-time (faster than using load-grunt-tasks)
        jitGrunt: {
            staticMappings: {
                'openui5_preload': 'grunt-openui5',
                'openui5_connect' : 'grunt-openui5',
                "openui5_theme" : "grunt-openui5",
                'jsdoc' : 'grunt-jsdoc'
            }
        },

        data: gruntData

    });
};