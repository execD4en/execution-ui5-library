sap.ui.define([
	'./CellFactory',
	'./ColumnFactory',
	'execution/base/table/BaseTableConfigIni',
	'sap/ui/table/Table'
],function(
	UiCellFactory,
	UiColumnFactory,
	BaseTableConfigIni,
	Table
){
	'use strict';
	
	/**
	 * constructor of class for build sap.ui.table.Table
	 * 
	 * 
	 * @param {object} oParam - settings
	 * @param {object} oParam.column - column settings
	 * @param {sap.ui.model.Model} oParam.column.model - column model
	 * @param {string} oParam.column.path - column path
	 * @param {object} oParam.row - row settings
	 * @param {sap.ui.model.Model} oParam.row.model - row model
	 * @param {string} oParam.row.path - row path
	 * @param {object} oParam.factory - factory settings
	 * @param {BaseElementFactory} oParam.factory.cell [ = BaseCellFactory] - cell factory
	 * @param {BaseElementFactory} oParam.factory.column [ = BaseMColumnFactory] - column factory
	 */
	var BaseTableConfiguratorIni = BaseTableConfigIni.extend('execution.base.table.ui.TableConfig',{
		
		constructor:function(oParam){
			
			if(!oParam.factory){
				oParam.factory = {};
			}
			
			if(!oParam.factory.cell){
				oParam.factory.cell = new UiCellFactory();
			}
			
			if(!oParam.factory.column){
				oParam.factory.column = new UiColumnFactory();
			}
			
			BaseTableConfigIni.prototype.constructor.apply(this, arguments);
		}
	});
		
		
	/**
	 * column binding 
	 */
	BaseTableConfiguratorIni.prototype.bindColumns = function(){
		var that = this;
		
		this.table().bindAggregation('columns', {
			model:this.column().name,
			path:this.column().path,
			factory:function(sId, oCtx){
				var oColumn = that.factory('column').getElement(oCtx);
				oColumn.setTemplate(that.factory('cell').getElement(oCtx, that));
				
				return oColumn;
			}
		});
	};
	
	/**
	 * items binding
	 */
	BaseTableConfiguratorIni.prototype.bindRows = function(){
		this.table().bindAggregation('rows',{
			model: this.row().name,
			path: this.row().path
		});
	};
	
	/**
	 * check table
	 */
	BaseTableConfiguratorIni.prototype.checkTable = function(oTable){
		if(!(oTable instanceof Table)){
			throw Error('Error. Table must be instance of sap.ui.table.Table');
		}
	};
	
	return BaseTableConfiguratorIni;
});