sap.ui.define([
	"sap/m/Text",
	"execution/base/factory/BaseAdapterFactory"
], function(
	Text,
	BaseAdapterFactory
) {
	"use strict";
	
	var oBaseAdapter = {
		field:'field',
		type:'type'
	};
	
	/**
	 * constructor for factory for cell with adapter
	 * 
	 * @class
	 */
	var UiTCellFactory = BaseAdapterFactory.extend("execution.base.table.ui.CellFactory", {
		constructor:function(){
			BaseAdapterFactory.prototype.constructor.call(this, oBaseAdapter);
		}
	});
	
	
	/**
	 * @description get ui for cell
	 * @param {sap.ui.model.Context} oClmnCtx - row context
	 * @param {sap.ui.model.Context} oRowCtx - column context
	 * @returns {sap.ui.core.Control} - control
	 */
	UiTCellFactory.prototype.getElement = function(oClmnCtx, oRowCtx) {
		
		var oClmnData = oClmnCtx.getObject();
		
		var cell = new Text({
			text: "{row>" + oClmnData[this.getAlias("field")] + "}"
		});

		return cell;
	};
	
	
	return UiTCellFactory;
});