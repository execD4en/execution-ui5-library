sap.ui.define([
	'sap/ui/table/Column',
	'execution/base/factory/BaseAdapterFactory',
	'sap/m/Text'
], function(
	Column,
	BaseAdapterFactory,
	Text
) {
	'use strict';
	
	var oBaseAdapter = {
		label:'label',
		hAlign:'hAlign',
		vAlign:'vAlign',
		width:'width'
	};
	
	
	/**  
	 * constructor of class for sap.ui.table.Column with adapter
	 * 
	 * @class
	 */
	var UiTColumnFactory = BaseAdapterFactory.extend('execution.base.table.ui.ColumnFactory', {
		
		constructor:function(){
			BaseAdapterFactory.prototype.constructor.apply(this, [oBaseAdapter]);
		}
	});
		
	
	/**
	 * get column
	 * @param {sap.ui.model.Context} oClmnCtx - column context of column model
	 * @returns {sap.ui.table.Column} - control
	 */
	UiTColumnFactory.prototype.getElement = function(oClmnCtx) {
			
		var oColumn = new Column({
			label: new Text({
				text: '{column>' + this.getAlias("label") + '}'
			}),
			hAlign:'{column>' + this.getAlias("hAlign") + '}',
			vAlign:'{column>' + this.getAlias("vAlign") + '}',
			width:'{column>' + this.getAlias("width") + '}'
		});

		return oColumn;
	};
	
	return UiTColumnFactory;
});