sap.ui.define([
	'execution/base/table/BaseTableConfig',
],function(
	BaseTableConfig
){
	'use strict';
	
	
	/**
	 * constructor for table configurator with initialization
	 * 
	 * @class
	 * @abstract
	 * 
	 * @param {object} oParam - settings
	 * @param {object} oParam.column - column settings
	 * @param {object} oParam.column.name  [='column'] - column model name
	 * @param {sap.ui.model.Model} oParam.column.model - column model
	 * @param {string} oParam.column.path - column path
	 * @param {object} oParam.row - row settings
	 * @param {object} oParam.row.name [='row'] - row model name
	 * @param {sap.ui.model.Model} oParam.row.model - row model
	 * @param {string} oParam.row.path - row path
	 * @param {object} oParam.factory - [= {}] factory map
	 */
	var BaseTableConfiguratorIni = BaseTableConfig.extend('execution.base.table.BaseTableConfigIni',{
		
		constructor:function(oParam){
			BaseTableConfig.prototype.constructor.apply(this, arguments);
			
			this._column = {};
			this._column.name = 'column';
			Object.assign(this._column, oParam.column);
			
			this._row = {};
			this._row.name = 'row';
			Object.assign(this._row, oParam.row);
			
			this._factory = {};
			Object.assign(this._factory, oParam.factory);
		}
	});
		
	/**
	 * get column info
	 * @returns {object} - map of column settings
	 */
	BaseTableConfiguratorIni.prototype.column = function(){
		return this._column;
	};
	
	/**
	 * get row info
	 * @returns {object} - map of row settings
	 */
	BaseTableConfiguratorIni.prototype.row = function(){
		return this._row;
	};
	
	/**
	 * get factory by name
	 * @returns {BaseElementFactory} - factory
	 */
	BaseTableConfiguratorIni.prototype.factory = function(name){
		return this._factory[name];
	};
	
	/**
	 * get table
	 * @returns {sap.ui.core.Control} table
	 */
	BaseTableConfiguratorIni.prototype.table = function(){
		return this._table;
	};
	
	
	/**
	 * build table
	 * @param {Table} - table
	 */
	BaseTableConfiguratorIni.prototype.build = function(oTable){
		
		this.checkTable(oTable);
		this._table = oTable;
		
		this._table.setModel(this._row.model, this._row.name);
		this._table.setModel(this._column.model, this._column.name);
		
		this.bindColumns();
		this.bindRows();
	};
		
	return BaseTableConfiguratorIni;
});