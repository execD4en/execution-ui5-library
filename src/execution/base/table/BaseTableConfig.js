sap.ui.define([
	'sap/ui/base/Object',
	'sap/ui/model/Model',
	'execution/base/factory/BaseElementFactory'
],function(
	BaseObject,
	Model,
	BaseElementFactory
){
	'use strict';
	
	/**
	 * constructor for table configurator
	 * 
	 * @class
	 * @abstract
	 * 
	 * @param {object} oParam - settings
	 * @param {object} oParam.column - column settings
	 * @param {sap.ui.model.Model} oParam.column.model - column model
	 * @param {string} oParam.column.path - column path
	 * @param {object} oParam.row - row settings
	 * @param {sap.ui.model.Model} oParam.row.model - row model
	 * @param {string} oParam.row.path - row path
	 * @param {object} oParam.factory - [= {}] factory map
	 */
	var BaseTableConfigurator = BaseObject.extend('execution.base.table.BaseTableConfig',{
		
		metadata : {
			abstract : true,
			publicMethods : [
				"bindRows", "bindColumns", "checkTable", "build"
			]
		},
		
		constructor:function(oParam){
			
			if(this.constructor === execution.base.table.BaseTableConfig){
				throw new Error("Abstract class	cannot be instantiated directly");
			}
			
			if(this.bindRows === undefined || 
					this.bindColumns === undefined || 
						this.checkTable === undefined ||
							this.build === undefined ){
				
				throw new Error("Redefine method. Abstract class extend");
			}
			
			this._checkParam(oParam);
		}
	});
		
		
	/**
	 * check input parameters
	 * @param {object} oParam - settings
	 * @private
	 */
	BaseTableConfigurator.prototype._checkParam = function(oParam){
		if (!oParam.row || !(oParam.row.model instanceof Model)) {
			throw Error("Error. Invalid instance of data model.");
		}
		
		if (!oParam.column || !(oParam.column.model instanceof Model)) {
			throw Error("Error. Invalid instance of data model.");
		}

		if (!oParam.column || !this._isPathOK(oParam.column.path)) {
			throw Error("Error. Invalid column path.");
		}

		if (!oParam.row || !this._isPathOK(oParam.row.path)) {
			throw Error("Error. Invalid row path.");
		}
		
		if (oParam.factory){
			for(var k in oParam.factory){
				if (!(oParam.factory[k] instanceof BaseElementFactory)) {
					throw Error("Error. Invalid instance of Factory. Extend BaseElementFactory!");
				}
			}
		}
	};
	
	/**
	 * check path
	 * @param {string} sPath - path for data in model
	 * @private
	 */
	BaseTableConfigurator.prototype._isPathOK = function(sPath){
		return $.type(sPath) === "string" && sPath.length > 0;
	};
	
	
	return BaseTableConfigurator;
});