sap.ui.define([
	'sap/m/Column',
	'execution/base/factory/BaseAdapterFactory',
	'sap/m/Text'
], function(
	Column,
	BaseAdapterFactory,
	Text
) {
	'use strict';
	
	var oBaseAdapter = {
		label:'label',
		hAlign:'hAlign',
		vAlign:'vAlign',
		width:'width'
	};
	
	/**  
	 * constructor 
	 * @class
	 */
	var MTColumnFactory = BaseAdapterFactory.extend('execution.base.table.m.ColumnFactory', {
			
		constructor:function(){
			BaseAdapterFactory.prototype.constructor.apply(this, [oBaseAdapter]);
		}
	});
	
	/**
	 * @description get column for sap.m.Table
	 * @param {sap.ui.model.Context} oClmnCtx - column context of column model
	 * @returns {sap.ui.core.Control} - control
	 */
	MTColumnFactory.prototype.getElement = function(oClmnCtx) {
		
		var oColumn = new Column({
			header: new Text({
				text: '{column>' + this.getAlias("label") + '}'
			}),
			hAlign:'{column>' + this.getAlias("hAlign") + '}',
			vAlign:'{column>' + this.getAlias("vAlign") + '}',
			width:'{column>' + this.getAlias("width") + '}'
		});

		return oColumn;
	};
	
	
	return MTColumnFactory;
});