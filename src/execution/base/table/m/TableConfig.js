sap.ui.define([
	'./CellFactory',
	'./ColumnFactory',
	'./CLIFactory',
	'execution/base/table/BaseTableConfigIni',
	'sap/m/Table'
],function(
	CellFactory,
	ColumnFactory,
	CLIFactory,
	BaseTableConfigIni,
	Table
){
	'use strict';
	
	/**
	 * constructor of class for build sap.m.Table
	 * 
	 * @class
	 * 
	 * @param {object} oParam - settings
	 * @param {object} oParam.column - column settings
	 * @param {object} oParam.column.name  [='column'] - column model name
	 * @param {sap.ui.model.Model} oParam.column.model - column model
	 * @param {string} oParam.column.path - column path
	 * @param {object} oParam.row - row settings
	 * @param {object} oParam.row.name [='row'] - row model name
	 * @param {sap.ui.model.Model} oParam.row.model - row model
	 * @param {string} oParam.row.path - row path
	 * @param {object} oParam.factory - factory settings
	 * @param {BaseElementFactory} oParam.factory.cell [ = CellFactory] - cell factory
	 * @param {BaseElementFactory} oParam.factory.column [ = MColumnFactory] - column factory
	 * @param {BaseElementFactory} oParam.factory.cli [ = ColumnListItem] - column list item factory
	 */
	var MTableConfigurator = BaseTableConfigIni.extend('execution.base.table.m.TableConfig',{
		
		constructor:function(oParam){
			
			if(!oParam.factory){
				oParam.factory = {};
			}
			
			if(!oParam.factory.cell){
				oParam.factory.cell = new CellFactory();
			}
			
			if(!oParam.factory.column){
				oParam.factory.column = new ColumnFactory();
			}
			
			if(!oParam.factory.cli){
				oParam.factory.cli = new CLIFactory();
			}
			
			BaseTableConfigIni.prototype.constructor.apply(this, arguments);
		}
	});
		
	
	/**
	 * column binding 
	 */
	MTableConfigurator.prototype.bindColumns = function(){
		var that = this;
		
		this.table().bindAggregation('columns', {
			model:this.column().name,
			path:this.column().path,
			factory:function(sId, oCtx){
				return that.factory('column').getElement(oCtx);
			}
		});
	};
	
	
	/**
	 * items binding
	 */
	MTableConfigurator.prototype.bindRows = function(){
		var that = this;
		
		this.table().bindAggregation('items',{
			model: this.row().name,
			path: this.row().path,
			factory:function(sId, oRowCtx){
				
				var oCli = that.factory('cli').getElement(oRowCtx);
				
				oCli.bindAggregation('cells',{
					model:that.column().name,
					path:that.column().path,
					factory: function(sId, oClmnCtx){
						return that.factory('cell').getElement(oClmnCtx, oRowCtx);
					}
				});
				
				return oCli;
			}
		});
	};
	
	
	/**
	 * check table
	 */
	MTableConfigurator.prototype.checkTable = function(oTable){
		if(!(oTable instanceof Table)){
			throw Error('Error. Table must be instance of sap.m.Table');
		}
	};
	
	
	return MTableConfigurator;
});