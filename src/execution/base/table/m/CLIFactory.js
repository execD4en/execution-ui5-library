sap.ui.define([
	'sap/m/ColumnListItem',
	'execution/base/factory/BaseElementFactory'
], function(
	ColumnListItem,
	BaseElementFactory
) {
	'use strict';
	
	/**
	 * constructor for factory of ColumnListItem 
	 */
	var MCLIFactory = BaseElementFactory.extend('execution.base.table.m.CLIFactory', {});
	
	/**
	 * @description get column list item for sap.m.Table
	 * @param {sap.ui.model.Context} oClmnCtx - row context of row model
	 * @returns {sap.ui.core.Control} - instance of ColumnListItem
	 */
	MCLIFactory.prototype.getElement = function(oRowCtx) {
		return new ColumnListItem();
	};
	
	return MCLIFactory;
});