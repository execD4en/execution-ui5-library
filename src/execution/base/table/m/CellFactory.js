sap.ui.define([
	"sap/m/Text",
	"execution/base/factory/BaseAdapterFactory"
], function(
	Text,
	BaseAdapterFactory
) {
	"use strict";
	
	var oBaseAdapter = {
		field:'field',
		type:'type'
	};
	
	
	/**
	 * constructor for cell 
	 */
	var MTCellFactory = BaseAdapterFactory.extend("execution.base.table.m.CellFactory", {
		
		constructor:function(){
			BaseAdapterFactory.prototype.constructor.call(this, oBaseAdapter);
		}
	});
	
	/**
	 * @description get ui for cell
	 * @param {sap.ui.model.Context} oClmnCtx - row context
	 * @param {sap.ui.model.Context} oRowCtx - column context
	 * @returns {sap.ui.core.Control} - control
	 */
	MTCellFactory.prototype.getElement = function(oClmnCtx, oRowCtx) {
		
		var oClmnData = oClmnCtx.getObject();
		
		var cell = new Text({
			text: "{row>" + oClmnData[this.getAlias("field")] + "}"
		});

		return cell;
	};
	
	
	return MTCellFactory;
});