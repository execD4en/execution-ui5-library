sap.ui.define([], 
	function(){
	
	return {
		getBundleText: function(sMessageId) {
			return sap.ui.getCore()
						 .getLibraryResourceBundle("execution.base")
						 .getText(sMessageId);
		},
		hasBundleText: function(sMessageId) {
			return sap.ui.getCore()
						 .getLibraryResourceBundle("execution.base")
						 .hasText(sMessageId);
		},
		getDataByPropertyKeyInArray: function(aData, sProperty, sKey){
			
			if ($.type(aData) !== 'array') return null;
			if ($.type(sProperty) !== 'string') return null;
			if ($.type(sKey) !== 'string') return null;
			
			var oData = null;
			
			jQuery.each(aData, function(index, oItem) {
				
				if (oItem[sProperty] === sKey) {
					oData = oItem;
					return false;
				}
				
			});
			
			return oData;
		}
	}
});