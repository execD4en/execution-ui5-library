sap.ui.define(['jquery.sap.global', 'sap/ui/core/library'],
    function (jQuery, library) {
        "use strict";

        sap.ui.getCore().initLibrary({
            name: "execution.base",
            version: "${version}",
            interfaces: [],
            controls: [],
            dependencies: ["sap.ui.core"],
//		noLibraryCSS: true,
            types: [
                "execution.base.odata.RequestType"
            ],
            elements: [
                "execution.base.exception.BaseException",
                "execution.base.exception.ODataException",
                "execution.base.dialog.BaseDialog",
                "execution.base.dialog.MessageDialog",
                "execution.base.dialog.ConfirmDialog",
                "execution.base.dialog.ValueHelpDialog",
                "execution.base.odata.RequestManager",
                "execution.base.factory.BaseAdapterFactory",
                "execution.base.factory.BaseElementFactory",
                "execution.base.table.BaseTableConfig",
                "execution.base.table.BaseTableConfigIni",
                "execution.base.table.m.CellFactory",
                "execution.base.table.m.CLIFactory",
                "execution.base.table.m.ColumnFactory",
                "execution.base.table.m.TableConfig",
                "execution.base.table.ui.CellFactory",
                "execution.base.table.ui.ColumnFactory",
                "execution.base.table.ui.TableConfig"
            ]
        });
        /**
         * @namespace execution.base
         * @author Execution
         * @public
         */
        var thisLib = execution.base;

        /**
         * @enum {string}
         * @description Тип запроса
         * @memberOf execution.base
         * @public
         */
        thisLib.odata.RequestType = {
            /**
             * @description Тип запроса GET
             * @public
             */
            GET: "GET",
            /**
             * @description Тип запроса POST
             * @public
             */
            POST: "POST"
        }


        return execution.base;

    });