sap.ui.define([
	'./BaseElementFactory'
],function(
	BaseElementFactory
){
	'use strict';
	
	
	/**
	 * constructor for factory with adapter
	 * 
	 * @class
	 * @abstract
	 * 
	 * @param {object} [oAdapter = {}]- map of alias for binding
	 */
	var AbsBaseAdapterFactory = BaseElementFactory.extend('execution.base.factory.BaseAdapterFactory',{
		
		constructor:function(oAdapter){
			BaseElementFactory.prototype.constructor.apply(this, arguments);
			
			if($.isPlainObject(oAdapter)){
				this._adapter = Object.create(oAdapter);
			}else{
				this._adapter = {};
			}
		}
	});
	
	
	/**
	 * set adapter for binding
	 * @parameter{object} oAdapter - map of alias for binding
	 */
	AbsBaseAdapterFactory.prototype.setAdapter = function(oAdapter){
		if($.isPlainObject(oAdapter)){
			Object.assign(this._adapter, oAdapter);
		}
	};
	
	
	/**
	 * get alias from adapter property
	 * @param{string} name - alias name
	 */
	AbsBaseAdapterFactory.prototype.getAlias = function(name){
		return this._adapter[name];
	};
	
	return AbsBaseAdapterFactory;
});