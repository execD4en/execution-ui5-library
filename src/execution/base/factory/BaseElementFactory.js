sap.ui.define([
	'sap/ui/base/Object'
],function(
	BaseObject
){
	'use strict';
	
	
	/**
	 * constructor for base factory
	 * 
	 * @class
	 * @abstract
	 */
	var AbsBaseElementFactory =  BaseObject.extend('execution.base.factory.BaseElementFactory',{
		
		metadata:{
			abstract:true,
			publicMethods: ["getElement"]
		},
		
		constructor:function(){
			
			if(this.constructor === execution.base.factory.BaseElementFactory){
				throw new Error("Abstract class	cannot be instantiated directly");
			}
			
			if(this.getElement === undefined){
				throw new Error("Redefine method. Abstract class extend")
			}
		}
		
	});
	
	
	return AbsBaseElementFactory;
});