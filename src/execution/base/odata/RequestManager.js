sap.ui.define(["sap/ui/base/Object",
        "execution/base/exception/ODataException",
        "execution/base/util/Utils",
        "execution/base/library"],
    function (Ui5Object,
              ODataException,
              Utils,
              library) {

        var RequestType = library.odata.RequestType;


        var RequestManager = Ui5Object.extend("execution.base.odata.RequestManager", {
            /**
             * @constructs RequestManager
             * @classdesc Класс для отправки запросов к серверу
             * @memberOf execution.base
             * @public
             * @description Конструктор класса
             * @param {(sap.ui.model.odata.ODataModel|sap.ui.model.odata.v2.ODataModel)} oDataModel - модель для odata-сервиса
             * @extends sap.ui.base.Object
             * @throws {execution.base.exception.ODataException} Ошибка. Не передана модель для получения данных с сервера!
             */
            constructor: function (oDataModel) {

                if (!(oDataModel instanceof sap.ui.model.odata.ODataModel)
                    && !(oDataModel instanceof sap.ui.model.odata.v2.ODataModel)) {
                    throw new ODataException(Utils.getBundleText("Ошибка. Не передана модель для получения данных с сервера!"));
                }
                /**
                 * @type {(sap.ui.model.odata.ODataModel|sap.ui.model.odata.v2.ODataModel)}
                 * @description Модель odata - сервиса
                 * @instance
                 * @private
                 */
                this._mainModel = oDataModel;
            }
        });

        //=============================================================================================================
        //=============================================================================================================
        // ПУБЛИЧНЫЕ МЕТОДЫ КЛАССА
        //=============================================================================================================
        //=============================================================================================================

        /**
         * @memberOf RequestManager
         * @public
         * @description Вернуть odata-модель
         * @returns {sap.ui.model.odata.ODataModel|sap.ui.model.odata.v2.ODataModel}
         */
        RequestManager.prototype.getModel = function () {
            return this._mainModel;
        }

        /**
         * @memberOf RequestManager
         * @description Совершить запрос к серверу
         * @public
         * @param {object} mParam - параметры для запроса
         * @param {string} mParam.serviceNode - сущность для запроса
         * @param {jQuery.Deferred} mParam.deferred - объект ожидания
         * @param {execution.base.odata.RequestType} mParam.requestType - тип запроса
         * @param {object} [mParam.data] - данные для запросов типа POST/PUT
         * @param {sap.ui.model.Filter[]} [mParam.filters] - фильтры для запроса
         * @param {object} [mParam.key] - объект с ключами для запроса
         * @param {object} [mParam.expandTree] - дерево для параметра $expand
         * @throws {execution.base.exception.ODataException} Ошибка. Не переданы обязательные параметры для выполнения запроса!
         * @throws {execution.base.exception.ODataException} Ошибка. Не передан узел для выполнения запроса!
         * @throws {execution.base.exception.ODataException} Ошибка. Такая сущность отсутствует в переданной модели!
         * @throws {execution.base.exception.ODataException} Ошибка. Не передан объект ожидания выполнения запроса!
         * @throws {execution.base.exception.ODataException} Ошибка. Не передан тип запроса!
         * @throws {execution.base.exception.ODataException} Ошибка. Передан некорректный тип запроса &1!
         *
         * @throws {execution.base.exception.ODataException} Ошибка. Не корректно определены данные для запроса!
         * @throws {execution.base.exception.ODataException} Ошибка. Отсутствует связка сущностей!
         *
         * @throws {execution.base.exception.ODataException} Ошибка. Не корректно определен ключ для запроса!
         * @throws {execution.base.exception.ODataException} Ошибка. Не корректно определен параметр $expand!
         * @throws {execution.base.exception.ODataException} Ошибка. Отсутствует связка сущностей в параметре $expand!
         * @throws {execution.base.exception.ODataException} Ошибка. Сущность &1 отсутствует в переданной модели!
         * @throws {execution.base.exception.ODataException} Ошибка. Не корректно определены фильтры для запроса!
         */
        RequestManager.prototype.doRequest = function (mParam) {
            this._checkRequestParam(mParam);

            var sType = mParam.requestType;

            switch (sType) {
                case RequestType.GET:
                    this._readData(mParam);
                    break;
                case RequestType.POST:
                    this._createData(mParam);
                    break;
            }
        }

        //=============================================================================================================
        //=============================================================================================================
        // ПРИВАТНЫЕ МЕТОДЫ КЛАССА
        //=============================================================================================================
        //=============================================================================================================

        /**
         * @memberOf RequestManager
         * @description Проверка входящих параметров
         * @private
         * @param {object} mParam - параметры для запроса
         * @param {string} mParam.serviceNode - сущность для запроса
         * @param {jQuery.Deferred} mParam.deferred - объект ожидания
         * @param {execution.base.odata.RequestType} mParam.requestType - тип запроса
         * @throws {execution.base.exception.ODataException} Ошибка. Не переданы обязательные параметры для выполнения запроса!
         * @throws {execution.base.exception.ODataException} Ошибка. Не передан узел для выполнения запроса!
         * @throws {execution.base.exception.ODataException} Ошибка. Такая сущность отсутствует в переданной модели!
         * @throws {execution.base.exception.ODataException} Ошибка. Не передан объект ожидания выполнения запроса!
         * @throws {execution.base.exception.ODataException} Ошибка. Не передан тип запроса!
         * @throws {execution.base.exception.ODataException} Ошибка. Передан некорректный тип запроса &1!
         */
        RequestManager.prototype._checkRequestParam = function (mParam) {

            if ($.type(mParam) !== 'object') {
                throw new ODataException("Ошибка. Не переданы обязательные параметры для выполнения запроса!");
            }

            if (!isPathOK(mParam.serviceNode)) {
                throw new ODataException("Ошибка. Не передан узел для выполнения запроса!");
            }

            if ($.type(this._getEntitySet(mParam.serviceNode)) === 'null') {
                throw new ODataException("Ошибка. Такая сущность отсутствует в переданной модели!");
            }

            if ($.type(mParam.deferred) !== 'object' || $.type(mParam.deferred.resolve) !== 'function') {
                throw new ODataException("Ошибка. Не передан объект ожидания выполнения запроса!");
            }

            if (!mParam.requestType) {
                throw new ODataException("Ошибка. Не передан тип запроса!");
            }

            if (mParam.requestType !== RequestType.GET
                && mParam.requestType !== RequestType.POST) {
                throw new ODataException("Ошибка. Передан некорректный тип запроса: " + mParam.requestType + " !");
            }

            function isPathOK(sPath) {
                return $.type(sPath) === "string" && sPath.length > 0;
            }
        }

        /**
         * @memberOf RequestManager
         * @description Совершить запрос типа POST к серверу
         * @private
         * @param {object} mParam - параметры для запроса
         * @param {string} mParam.serviceNode - сущность для запроса
         * @param {jQuery.Deferred} mParam.deferred - объект ожидания
         * @param {execution.base.odata.RequestType} mParam.requestType - тип запроса
         * @param {object} mParam.data - данные для запросов типа POST/PUT
         * @throws {execution.base.exception.ODataException} Ошибка. Не корректно определены данные для запроса!
         * @throws {execution.base.exception.ODataException} Ошибка. Отсутствует связка сущностей!
         * @throws {execution.base.exception.ODataException} Ошибка. Такая сущность отсутствует в переданной модели!
         */
        RequestManager.prototype._createData = function (mParam) {
            this._checkRequestCreateParam(mParam);

            var oDeferred = mParam.deferred;
            var sNode = mParam.serviceNode;
            var oData = mParam.data;

            var sPath = "/" + sNode;

            this.getModel()
                .create(sPath, oData, {
                    success: function (data) {
                        oDeferred.resolve(data);
                    },
                    error: function () {
                        oDeferred.reject();
                    }
                });
        }

        /**
         * @memberOf RequestManager
         * @private
         * @description Проверить параметры для запроса типа POST
         * @param {object} mParam - объект с параметрами для проверки
         * @param {object} mParam.data - объект с данными для отправки
         * @throws {execution.base.exception.ODataException} Ошибка. Не корректно определены данные для запроса!
         * @throws {execution.base.exception.ODataException} Ошибка. Отсутствует связка сущностей!
         * @throws {execution.base.exception.ODataException} Ошибка. Такая сущность отсутствует в переданной модели!
         */
        RequestManager.prototype._checkRequestCreateParam = function (mParam) {

            this._checkRequestData(mParam.serviceNode, mParam.data);
        }

        /**
         * @memberOf RequestManager
         * @private
         * @description Проверить параметры для запроса типа POST
         * @param {string} sNode - сущность запроса
         * @param {object} oData - объект с данными для отправки
         * @throws {execution.base.exception.ODataException} Ошибка. Не корректно определены данные для запроса!
         * @throws {execution.base.exception.ODataException} Ошибка. Отсутствует связка сущностей!
         * @throws {execution.base.exception.ODataException} Ошибка. Такая сущность отсутствует в переданной модели!
         */
        RequestManager.prototype._checkRequestData = function (sNode, oData) {

            var that = this;
            //Данные не переданы
            if ($.type(oData) !== 'object') {
                throw new ODataException("Ошибка. Не корректно определены данные для запроса!");
            }

            var oEntityInfo = this._getEntitySet(sNode);

            if ($.type(oEntityInfo) === 'null') {
                throw new ODataException("Ошибка. Такая сущность отсутствует в переданной модели!");
            }

            var sEntityType = oEntityInfo.entityType;

            var oEntityType = this._getEntityType(sEntityType);
            //Проверка на существование сущности
            if ($.type(oEntityType) === 'null') {
                throw new ODataException("Ошибка. Такая сущность отсутствует в переданной модели!");
            }
            //Список свойств сущности
            var aProperties = oEntityInfo.property;
            //Проверяем каждое свойство
            for (var sKey in oData) {
                //Заведомо ошибочное значение
                if (($.type(oData[sKey]) === 'null')
                    || ($.type(oData[sKey]) === 'undefined')) {
                    throw new ODataException("Ошибка. Не корректно определены данные для запроса!");
                }
                //Свойство - есть массив -> проверяем NavigationProperty
                if ($.type(oData[sKey]) === 'array') {

                    var sNavProperty = sKey;
                    //Ищем сущность соответствующую NavigationProperty
                    var oCurEntitySet = that._getEntitySetByNavProperty(oEntityType, sNavProperty);

                    //Не нашли -> связка отсутствует
                    if ($.type(oCurEntitySet) === 'null') {
                        throw new ODataException("Ошибка. Отсутствует связка сущностей!");
                    }
                    //Массив: проверяем каждый объект
                    jQuery.each(oData[sKey], function (index, oItem) {
                        that._checkRequestData(oCurEntitySet.name, oItem);
                    });

                    continue;
                }
                //Проверяем, что такое свойство существует в сущности
                if ($.type(Utils.getDataByPropertyKeyInArray(aProperties, "name", sKey)) === 'null') {
                    throw new ODataException("Ошибка. Не корректно определены данные для запроса!");
                }

            }

        }

        /**
         * @memberOf RequestManager
         * @description Совершить запрос типа GET к серверу
         * @private
         * @param {object} mParam - параметры для запроса
         * @param {string} mParam.serviceNode - сущность для запроса
         * @param {jQuery.Deferred} mParam.deferred - объект ожидания
         * @param {execution.base.odata.RequestType} mParam.requestType - тип запроса
         * @param {sap.ui.model.Filter[]} [mParam.filters] - фильтры для запроса
         * @param {object} [mParam.key] - объект с ключами для запроса
         * @param {object} [mParam.expandTree] - дерево для параметра $expand
         * @throws {execution.base.exception.ODataException} Ошибка. Не корректно определен ключ для запроса!
         * @throws {execution.base.exception.ODataException} Ошибка. Не корректно определен параметр $expand!
         * @throws {execution.base.exception.ODataException} Ошибка. Отсутствует связка сущностей в параметре $expand!
         * @throws {execution.base.exception.ODataException} Ошибка. Сущность &1 отсутствует в переданной модели!
         * @throws {execution.base.exception.ODataException} Ошибка. Не корректно определены фильтры для запроса!
         */
        RequestManager.prototype._readData = function (mParam) {

            this._checkRequestReadParam(mParam);

            var oModel = this.getModel();

            var oDeferred = mParam.deferred;
            var sNode = mParam.serviceNode;
            var aFilters = ($.type(mParam.filters) === 'array') ? mParam.filters : [];
            var oKey = ($.type(mParam.key) === 'object') ? mParam.key : null;

            var oUrlParam = this._createUrlParameters(mParam) || {};

            var sPath = "/" + sNode;

            if ($.type(oKey) === 'object') {
                sPath = "/" + oModel.createKey(sNode, oKey);
            }

            this.getModel()
                .read(sPath, {
                    urlParameters: oUrlParam,
                    filters: aFilters,
                    success: function (data) {
                        oDeferred.resolve(data);
                    },
                    error: function () {
                        oDeferred.reject();
                    }
                });
        }

        /**
         * @memberOf RequestManager
         * @description Проверить параметры для запроса типа GET
         * @private
         * @param {object} mParam - параметры для запроса
         * @param {string} mParam.serviceNode - сущность для запроса
         * @param {sap.ui.model.Filter[]} [mParam.filters] - фильтры для запроса
         * @param {object} [mParam.key] - объект с ключами для запроса
         * @param {object} [mParam.expandTree] - дерево для параметра $expand
         * @throws {execution.base.exception.ODataException} Ошибка. Не корректно определен ключ для запроса!
         *
         * @throws {execution.base.exception.ODataException} Ошибка. Не корректно определен параметр $expand!
         * @throws {execution.base.exception.ODataException} Ошибка. Отсутствует связка сущностей в параметре $expand!
         * @throws {execution.base.exception.ODataException} Ошибка. Сущность &1 отсутствует в переданной модели!
         *
         * @throws {execution.base.exception.ODataException} Ошибка. Не корректно определены фильтры для запроса!
         */
        RequestManager.prototype._checkRequestReadParam = function (mParam) {

            this._checkFilters(mParam.serviceNode, mParam.filters);
            this._checkKey(mParam.serviceNode, mParam.key);
            this._checkExpandTree(mParam.serviceNode, mParam.expandTree);

        }

        /**
         * @memberOf RequestManager
         * @description Проверить объект для ключа запроса
         * @private
         * @param {string} sNode - сущность для запроса
         * @param {object} [oKey] - объект с ключами для запроса
         * @throws {execution.base.exception.ODataException} Ошибка. Не корректно определен ключ для запроса!
         */
        RequestManager.prototype._checkKey = function (sNode, oKey) {

            if ($.type(oKey) === 'undefined') return;

            if ($.type(oKey) !== 'object') {
                throw new ODataException("Ошибка. Не корректно определен ключ для запроса!");
            }

            for (var sKey in oKey) {

                if ($.type(this._getEntityKey(sNode, sKey)) === 'null') {
                    throw new ODataException("Ошибка. Не корректно определен ключ для запроса!");
                }

            }
        }

        /**
         * @memberOf RequestManager
         * @description Проверить параметр $expand для запроса
         * @private
         * @param {string} sNode - сущность для запроса
         * @param {object} [oExpandTree] - дерево для параметра $expand
         * @throws {execution.base.exception.ODataException} Ошибка. Не корректно определен параметр $expand!
         * @throws {execution.base.exception.ODataException} Ошибка. Отсутствует связка сущностей в параметре $expand!
         * @throws {execution.base.exception.ODataException} Ошибка. Сущность &1 отсутствует в переданной модели!
         */
        RequestManager.prototype._checkExpandTree = function (sNode, oExpandTree) {
            //Параметр не передан
            if ($.type(oExpandTree) === 'undefined') return;

            var that = this;
            //Запускаем реккурсию по дереву
            checkTree(sNode, oExpandTree);

            function checkTree(sParentKey, oTree) {
                //Некорректный тип параметра
                if ($.type(oTree) !== 'object') {
                    throw new ODataException("Ошибка. Не корректно определен параметр $expand!");
                }
                //Передан пустой объект
                if (Object.keys(oTree).length === 0) {
                    throw new ODataException("Ошибка. Не корректно определен параметр $expand!");
                }
                //Сущность предка
                var oParentEntitySet = that._getEntitySet(sParentKey);

                if ($.type(oParentEntitySet) === 'null') {
                    throw new ODataException("Ошибка. Сущность" + sParentKey + "отсутствует в переданной модели!");
                }

                var sParentEntityType = oParentEntitySet.entityType;

                var oParentEntityType = that._getEntityType(sParentEntityType);

                if ($.type(oParentEntityType) === 'null') {
                    throw new ODataException("Ошибка. Сущность" + sParentKey + "отсутствует в переданной модели!");
                }
                //Дерево состоит из параметров NavigationProperty
                //Проверяем каждый на связку с предком
                for (var sNavProperty in oTree) {
                    //Ищем сущность соответствующую NavigationProperty
                    var oEntitySet = that._getEntitySetByNavProperty(oParentEntityType, sNavProperty);
                    //Не нашли -> связка отсутствует
                    if ($.type(oEntitySet) === 'null') {
                        throw new ODataException("Ошибка. Отсутствует связка сущностей в параметре $expand!");
                    }
                    //Потомков у узла нет
                    if ($.type(oTree[sNavProperty]) === 'null') continue;
                    //Запускаем анализ потомков
                    checkTree(oEntitySet.name, oTree[sNavProperty]);
                }
            }


        }

        /**
         * @memberOf RequestManager
         * @description Проверить фильтры для запроса
         * @private
         * @param {string} sNode - сущность для запроса
         * @param {sap.ui.model.Filter[]} [aFilters] - фильтры для запроса
         * @throws {execution.base.exception.ODataException} Ошибка. Не корректно определены фильтры для запроса!
         */
        RequestManager.prototype._checkFilters = function (sNode, aFilters) {

            if ($.type(aFilters) === 'undefined') return;

            if ($.type(aFilters) !== 'array') {
                throw new ODataException("Ошибка. Не корректно определены фильтры для запроса!");
            }

            var that = this;

            jQuery.each(aFilters, function (index, oFilter) {

                if (!(oFilter instanceof sap.ui.model.Filter)) {
                    throw new ODataException("Ошибка. Не корректно определены фильтры для запроса!");
                }

                if (($.type(oFilter.sPath) === 'undefined')
                    || ($.type(oFilter.sOperator) === 'undefined')
                    || ($.type(oFilter.oValue1) === 'undefined')) {
                    throw new ODataException("Ошибка. Не корректно определены фильтры для запроса!");
                }

                if ($.type(that._getEntityProperty(sNode, oFilter.sPath)) === 'null') {
                    throw new ODataException("Ошибка. Не корректно определены фильтры для запроса!");
                }

            });

        }

        /**
         * @memberOf RequestManager
         * @description Создать объект urlParameters для запроса
         * @private
         * @param {object} mParam - параметры для запроса
         * @param {object} [mParam.expandTree] - дерево для параметра $expand
         * @returns {object} oUrlParam - объект urlParameters
         */
        Request.prototype._createUrlParameters = function (mParam) {
            var oUrlParam = {};

            var oExpandTree = ($.type(mParam.expandTree) === 'object') ? mParam.expandTree : null;

            if ($.type(oExpandTree) === 'object') {
                var sExpand = this._createExpand(oExpandTree);

                if ($.type(sExpand) === 'string') {
                    oUrlParam.$expand = sExpand;
                }
            }

            return oUrlParam;
        }

        /**
         * @memberOf RequestManager
         * @description Сформировать строку для параметра $expand
         * @private
         * @param {object} oExpandTree - дерево для параметра $expand
         * @returns {string|null} sExpand - строка для параметра $expand
         */
        RequestManager.prototype._createExpand = function (oExpandTree) {

            var aExpandData = formExpand(oExpandTree);

            if (aExpandData.length === 0) return null;

            var aResults = jQuery.map(aExpandData, function (anyItem, index) {
                if ($.type(anyItem) === 'string') {
                    return anyItem;
                }

                return anyItem.join("/");
            });

            var sExpand = aResults.join(",");

            return sExpand;

            function formExpand(oData) {

                if ($.type(oData) !== 'object') return [];

                var aData = [];

                for (var sKey in oData) {
                    if ($.type(oData[sKey]) !== 'object') {
                        aData.push(sKey);
                        continue;
                    }

                    var aBranchData = formExpand(oData[sKey]);

                    if (aBranchData.length === 0) {
                        aData.push(sKey);
                        continue;
                    }

                    jQuery.each(aBranchData, function (index, anyBranch) {
                        var aCurrentData = [];

                        if ($.type(anyBranch) === 'string') {
                            aCurrentData.push(sKey, anyBranch);

                            aData.push(aCurrentData);
                            return true;
                        }

                        if ($.type(anyBranch) === 'array') {
                            aCurrentData.push(sKey);

                            aCurrentData = aCurrentData.concat(anyBranch);

                            aData.push(aCurrentData);
                            return true;
                        }
                    });
                }

                return aData;
            }

        }

        /**
         * @memberOf RequestManager
         * @description Найти EntitySet по сущности в метаданных
         * @private
         * @param {string} sEntitySet - сущность запроса
         * @returns {object|null} oEntityInfo - объект EntitySet
         */
        RequestManager.prototype._getEntitySet = function (sEntitySet) {
            var oModel = this.getModel();

            var oEntityInfo = oModel.oMetadata._getEntityTypeByPath(sEntitySet);

            return ($.type(oEntityInfo) === 'undefined') ? null : oEntityInfo;
        }

        /**
         * @memberOf RequestManager
         * @description Найти EntitySet по EntityType
         * @private
         * @param {object} oEntityType - EntityType сущности
         * @returns {object|null} oEntityInfo - объект EntitySet
         */
        RequestManager.prototype._getEntitySetByType = function (oEntityType) {
            var oModel = this.getModel();

            return oModel.oMetadata._getEntitySetByType(oEntityType);
        }

        /**
         * @memberOf RequestManager
         * @description Найти свойство в сущности
         * @private
         * @param {string} sEntitySet - сущность запроса
         * @param {string} sProperty - свойство сущности
         * @returns {object|null} Объект с информацией о свойстве
         */
        RequestManager.prototype._getEntityProperty = function (sEntitySet, sProperty) {
            var oModel = this.getModel();

            var oEntityInfo = this._getEntitySet(sEntitySet);

            if ($.type(oEntityInfo) === 'null') return null;

            return Utils.getDataByPropertyKeyInArray(oEntityInfo.property, "name", sProperty);
        }

        /**
         * @memberOf RequestManager
         * @description Найти EntityType по ключу типа сущности
         * @private
         * @param {string} sType - ключ для EntityType
         * @returns {object|null} oEntityInfo - EntityType сущности
         */
        RequestManager.prototype._getEntityType = function (sType) {
            var oModel = this.getModel();

            var oEntityInfo = oModel.oMetadata._getEntityTypeByName(sType);

            return ($.type(oEntityInfo) === 'undefined') ? null : oEntityInfo;
        }


        /**
         * @memberOf RequestManager
         * @description Найти сущность по NavigationProperty и EntityType отца
         * @private
         * @param {object} oEntityType - объект EntityType отца
         * @param {string} sProperty - NavigationProperty
         * @returns {object|null} Объект EntitySet сущности
         */
        RequestManager.prototype._getEntitySetByNavProperty = function (oEntityType, sProperty) {
            var oModel = this.getModel();

            var oEntityType = oModel.oMetadata._getEntityTypeByNavProperty(oEntityType, sProperty);

            if ($.type(oEntityType) === 'undefined') return null;

            return this._getEntitySetByType(oEntityType);
        }

        /**
         * @memberOf RequestManager
         * @description Проверка, что свойство сущности является ключевым
         * @private
         * @param {string} sEntitySet - сущность запроса
         * @param {string} sKey - свойство сущности
         * @returns {object|null} Объект с ключом сущности
         */
        RequestManager.prototype._getEntityKey = function (sEntitySet, sKey) {
            var oModel = this.getModel();

            var oEntityInfo = this._getEntitySet(sEntitySet);

            if ($.type(oEntityInfo) === 'null') return null;

            return Utils.getDataByPropertyKeyInArray(oEntityInfo.key.propertyRef, "name", sKey);
        }


        return RequestManager;

    });
