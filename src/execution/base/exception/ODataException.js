sap.ui.define(["./BaseException"],
    function (BaseException) {

        /**
         * @classdesc Класс исключений для ошибок при запросах к серверу через odata-модель
         * @description Конструктор класса
         * @public
         * @constructs ODataException
         * @param {string} message - сообщение об ошибке
         * @memberOf execution.base
         * @extends BaseException
         */
        function ODataException(message) {
            var instance = new BaseException(message);

            /**
             * @description Название ошибки, созданной через класс исключений
             * @memberOf ODataException
             * @type {string}
             * @name name
             * @instance
             */
            instance.name = "ODataError";

            Object.setPrototypeOf(instance, Object.getPrototypeOf(this));
            return instance;
        }

        ODataException.prototype = Object.create(BaseException.prototype, {
            constructor: {
                value: BaseException,
                enumerable: false,
                writable: true,
                configurable: true
            }
        });

        if (Object.setPrototypeOf) {
            Object.setPrototypeOf(ODataException, BaseException);
        } else {
            ODataException.__proto__ = BaseException;
        }

        return ODataException;
    });