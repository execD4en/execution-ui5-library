sap.ui.define([],
    function () {

        "use strict";

        /**
         * @classdesc Базовый класс исключений библиотеки
         * @description Конструктор класса
         * @public
         * @constructs BaseException
         * @param {string} message - сообщение об ошибке
         * @memberOf execution.base
         * @extends Error
         */
        function Exception(message) {
            var instance = new Error(message);
            /**
             * @description Название ошибки, созданной через класс исключений
             * @memberOf BaseException
             * @type {string}
             * @name name
             * @instance
             */
            instance.name = "BaseError";

            Object.setPrototypeOf(instance, Object.getPrototypeOf(this));
            return instance;
        }

        Exception.prototype = Object.create(Error.prototype, {
            constructor: {
                value: Error,
                enumerable: false,
                writable: true,
                configurable: true
            }
        });

        if (Object.setPrototypeOf) {
            Object.setPrototypeOf(Exception, Error);
        } else {
            Exception.__proto__ = Error;
        }

        return Exception;
    });