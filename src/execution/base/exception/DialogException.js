sap.ui.define(["./BaseException"],
    function (BaseException) {

        /**
         * @classdesc Класс исключений для ошибок при создании диалогового окна
         * @description Конструктор класса
         * @public
         * @constructs DialogException
         * @param {string} message - сообщение об ошибке
         * @memberOf execution.base
         * @extends BaseException
         */
        function DialogException(message) {
            var instance = new BaseException(message);

            /**
             * @description Название ошибки, созданной через класс исключений
             * @memberOf DialogException
             * @type {string}
             * @name name
             * @instance
             */
            instance.name = "DialogError";

            Object.setPrototypeOf(instance, Object.getPrototypeOf(this));
            return instance;
        }

        DialogException.prototype = Object.create(BaseException.prototype, {
            constructor: {
                value: BaseException,
                enumerable: false,
                writable: true,
                configurable: true
            }
        });

        if (Object.setPrototypeOf) {
            Object.setPrototypeOf(DialogException, BaseException);
        } else {
            DialogException.__proto__ = BaseException;
        }

        return DialogException;
    });