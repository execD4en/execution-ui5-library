sap.ui.define([
        "sap/ui/base/Object",
        "execution/base/exception/DialogException",
        "execution/base/util/Utils"
    ],
    function (Ui5Object,
              DialogException,
              Utils) {

        var DLG_EVENT_AFTER_CLOSE = "afterClose",
            BTN_CLOSE_ID = "close";

        var BaseDialog = Ui5Object.extend("execution.base.dialog.BaseDialog", {
            /**
             * @constructs BaseDialog
             * @classdesc Класс для построения базового диалогового окна
             * @memberOf execution.base
             * @public
             * @description Конструктор класса
             * @param {object} [mParam] - объект с параметрами
             * @param {sap.ui.core.CSSSize} [mParam.width] - ширина окна
             * @param {sap.ui.core.CSSSize} [mParam.height] - высота окна
             * @param {string} [mParam.title] - заголовок окна
             * @param {boolean} [mParam.resizable] - индикатор: можно менять размер окна
             * @param {boolean} [mParam.draggable] - индикатор: окно можно двигать
             * @param {jQuery.Deferred} [mParam.deferred] - объект ожидания
             * @param {array} [mParam.buttons] - массив данных по кнопкам
             * @param {array} [mParam.events] - массив событий для диалогового окна
             * @extends sap.ui.base.Object
             * @throws {execution.base.exception.DialogException} Ошибка. Не корректно передан заголовок диалогового окна!
             * @throws {execution.base.exception.DialogException} Ошибка. Не корректно передан объект ожидания!
             * @throws {execution.base.exception.DialogException} Ошибка. Не корректно передана ширина диалогового окна!
             * @throws {execution.base.exception.DialogException} Ошибка. Не корректно передана высота диалогового окна!
             * @throws {execution.base.exception.DialogException} Ошибка. Не корректно определены события для диалогового окна!
             * @throws {execution.base.exception.DialogException} Ошибка. Не корректно определены кнопки для диалогового окна!
             */
            constructor: function (mParam) {

                if ($.type(mParam) !== 'object') mParam = {};

                /**
                 * @description События диалогового окна
                 * @type {array}
                 * @instance
                 * @private
                 */
                this._events = null;
                /**
                 * @description Массив кнопок диалогового окна
                 * @type {array}
                 * @instance
                 * @private
                 */
                this._buttons = null;

                this._checkParam(mParam);
                /**
                 * @description Объект диалогового окна
                 * @type {sap.m.Dialog}
                 * @instance
                 * @private
                 */
                this._dialog = null;
                /**
                 * @description Ширина окна
                 * @type {sap.ui.core.CSSSize}
                 * @instance
                 * @private
                 */
                this._width = mParam.width || "auto";
                /**
                 * @description Высота окна
                 * @type {sap.ui.core.CSSSize}
                 * @instance
                 * @private
                 */
                this._height = mParam.height || "auto";
                /**
                 * @description Заголовок окна
                 * @type {string}
                 * @instance
                 * @private
                 */
                this._title = mParam.title || Utils.getBundleText("BASE_DIALOG_TITLE");
                /**
                 * @description Индикатор: можно менять размер окна
                 * @type {boolean}
                 * @instance
                 * @private
                 */
                this._resizable = ($.type(mParam.resizable) === 'boolean') ? mParam.resizable : false;
                /**
                 * @description Индикатор: окно можно двигать
                 * @type {boolean}
                 * @instance
                 * @private
                 */
                this._draggable = ($.type(mParam.draggable) === 'boolean') ? mParam.draggable : false;
                /**
                 * @description Объект ожидания
                 * @type {jQuery.Deferred|null}
                 * @instance
                 * @private
                 */
                this._deferred = mParam.deferred || null;
            }
        });

        //=============================================================================================================
        //=============================================================================================================
        // СОБЫТИЯ КЛАССА
        //=============================================================================================================
        //=============================================================================================================

        /**
         * @memberOf BaseDialog
         * @public
         * @description Событие: после закрытия диалогового окна
         * @param {sap.ui.base.Event} oEvent - объект события
         */
        BaseDialog.prototype.onAfterClose = function (oEvent) {
            this.destroy();
        }

        /**
         * @memberOf BaseDialog
         * @public
         * @description Событие: закрытие диалогового окна
         * @param {sap.ui.base.Event} oEvent - объект событя
         */
        BaseDialog.prototype.onClose = function (oEvent) {
            var oDialog = this.getDialog();
            var oDeferred = this.getDeferred();

            if ($.type(oDeferred) !== 'null') oDeferred.resolve();

            oDialog.close();
        }

        //=============================================================================================================
        //=============================================================================================================
        // ПУБЛИЧНЫЕ МЕТОДЫ КЛАССА
        //=============================================================================================================
        //=============================================================================================================

        /**
         * @memberOf BaseDialog
         * @public
         * @description Вернуть массив событий
         * @returns {array} События диалогового окна
         */
        BaseDialog.prototype.getEvents = function () {
            return this._events;
        }

        /**
         * @memberOf BaseDialog
         * @public
         * @description Сеттер для событий диалогового окна
         * @param {array} aData - cобытия диалогового окна
         */
        BaseDialog.prototype.setEvents = function (aData) {
            this._events = aData;
        }

        /**
         * @memberOf BaseDialog
         * @public
         * @description Сеттер для кнопок диалогового окна
         * @returns {array} Кнопки диалогового окна
         */
        BaseDialog.prototype.getButtons = function () {
            return this._buttons;
        }

        /**
         * @memberOf BaseDialog
         * @public
         * @description Сеттер для кнопок диалогового окна
         * @param {array} aData - кнопки диалогового окна
         */
        BaseDialog.prototype.setButtons = function (aData) {
            this._buttons = aData;
        }

        /**
         * @memberOf BaseDialog
         * @public
         * @description Вернуть ширину диалогового окна
         * @returns {sap.ui.core.CSSSize} Ширина диалогового окна
         */
        BaseDialog.prototype.getWidth = function () {
            return this._width;
        }

        /**
         * @memberOf BaseDialog
         * @public
         * @description Вернуть высоту диалогового окна
         * @returns {sap.ui.core.CSSSize} Высота диалогового окна
         */
        BaseDialog.prototype.getHeight = function () {
            return this._height;
        }

        /**
         * @memberOf BaseDialog
         * @public
         * @description Вернуть заголовок диалогового окна
         * @returns {string} Заголовок диалогового окна
         */
        BaseDialog.prototype.getTitle = function () {
            return this._title;
        }

        /**
         * @memberOf BaseDialog
         * @public
         * @description Вернуть индикатор возможности менять размер диалогового окна
         * @returns {boolean} Возможность менять размер окна
         */
        BaseDialog.prototype.getResizable = function () {
            return this._resizable;
        }

        /**
         * @memberOf BaseDialog
         * @public
         * @description Вернуть индикатор возможности двигать диалоговое окна
         * @returns {boolean} Возможность двигать окно
         */
        BaseDialog.prototype.getDraggable = function () {
            return this._draggable;
        }

        /**
         * @memberOf BaseDialog
         * @public
         * @description Вернуть объект диалогового окна
         * @returns {sap.m.Dialog} Диалоговое окно
         */
        BaseDialog.prototype.getDialog = function () {
            return this._dialog;
        }

        /**
         * @memberOf BaseDialog
         * @public
         * @description Вернуть объект ожидания
         * @returns {jQuery.Deferred} Объект ожидания
         */
        BaseDialog.prototype.getDeferred = function () {
            return this._deferred;
        }

        /**
         * @memberOf BaseDialog
         * @public
         * @description Очистить переменные класса
         */
        BaseDialog.prototype.destroy = function () {

            var oDialog = this.getDialog();

            oDialog.destroy();

            this._dialog = oDialog =  null;

            this._buttons = null;
            this._events = null;

            Ui5Object.prototype.destroy.apply(this, arguments);
        }

        /**
         * @memberOf BaseDialog
         * @public
         * @description Построить диалоговое окно по его параметрам
         * @returns {sap.m.Dialog} Объект диалогового окна
         */
        BaseDialog.prototype.build = function () {

            this._dialog = new sap.m.Dialog({
                contentWidth: this.getWidth(),
                contentHeight: this.getHeight(),
                title: this.getTitle(),
                resizable: this.getResizable(),
                draggable: this.getDraggable()
            }).addStyleClass("execBaseDlg");

            this._attachEvents();
            this._applyButtons();

            return this._dialog;
        }

        //=============================================================================================================
        //=============================================================================================================
        // ПРИВАТНЫЕ МЕТОДЫ КЛАССА
        //=============================================================================================================
        //=============================================================================================================

        /**
         * @memberOf BaseDialog
         * @private
         * @description Добавить текущие кнопки в диалоговое окно
         */
        BaseDialog.prototype._applyButtons = function () {

            var that = this;

            var aButtons = this.getButtons();
            var oDialog = this.getDialog();

            jQuery.each(aButtons, function (i, oButton) {

                var oElement = null;

                switch (oButton.id) {
                    case BTN_CLOSE_ID:

                        oElement = new sap.m.Button({
                            text: oButton.text,
                            type: oButton.type,
                            press: that.onClose.bind(that)
                        });
                        break;
                    default:
                        return true;
                }

                oDialog.addButton(oElement);
            });

        }

        /**
         * @memberOf BaseDialog
         * @private
         * @description Привязать события к диалоговому окну
         */
        BaseDialog.prototype._attachEvents = function () {

            var that = this;

            var aEvents = this.getEvents();
            var oDialog = this.getDialog();

            jQuery.each(aEvents, function (i, oEvent) {

                switch (oEvent.id) {
                    case DLG_EVENT_AFTER_CLOSE:
                        oDialog.attachAfterClose(that.onAfterClose, that);
                        break;
                }

            });

        }

        /**
         * @memberOf BaseDialog
         * @private
         * @description Проверить все входящие параметры
         * @param {object} mParam - объект с параметрами
         * @param {sap.ui.core.CSSSize} [mParam.width] - ширина окна
         * @param {sap.ui.core.CSSSize} [mParam.height] - высота окна
         * @param {string} [mParam.title] - заголовок окна
         * @param {jQuery.Deferred} [mParam.deferred] - объект ожидания
         * @param {array} [mParam.buttons] - массив данных по кнопкам
         * @param {array} [mParam.events] - массив событий для диалогового окна
         * @throws {execution.base.exception.DialogException} Ошибка. Не корректно передан заголовок диалогового окна!
         * @throws {execution.base.exception.DialogException} Ошибка. Не корректно передан объект ожидания!
         * @throws {execution.base.exception.DialogException} Ошибка. Не корректно передана ширина диалогового окна!
         * @throws {execution.base.exception.DialogException} Ошибка. Не корректно передана высота диалогового окна!
         * @throws {execution.base.exception.DialogException} Ошибка. Не корректно определены события для диалогового окна!
         * @throws {execution.base.exception.DialogException} Ошибка. Не корректно определены кнопки для диалогового окна!
         */
        BaseDialog.prototype._checkParam = function (mParam) {

            this._checkWidth(mParam.width);
            this._checkHeight(mParam.height);
            this._checkTitle(mParam.title);
            this._checkDeferred(mParam.deferred);

            this._checkButtons(mParam.buttons);
            this._checkEvents(mParam.events);

        }

        /**
         * @memberOf BaseDialog
         * @description Проверить заголовок диалогового окна
         * @private
         * @param {string} [sTitle] - заголовок диалогового окна
         * @throws {execution.base.exception.DialogException} Ошибка. Не корректно передан заголовок диалогового окна!
         */
        BaseDialog.prototype._checkTitle = function (sTitle) {

            if ($.type(sTitle) === 'undefined') return;

            if ($.type(sTitle) !== 'string') {
                throw new DialogException("Ошибка. Не корректно передан заголовок диалогового окна!");
            }

        }

        /**
         * @memberOf BaseDialog
         * @description Проверить объект ожидания
         * @private
         * @param {jQuery.Deferred} [oDeferred] - объект ожидания
         * @throws {execution.base.exception.DialogException} Ошибка. Не корректно передан объект ожидания!
         */
        BaseDialog.prototype._checkDeferred = function (oDeferred) {

            if ($.type(oDeferred) === 'undefined') return;

            if ($.type(oDeferred) !== 'object'
                || $.type(oDeferred.resolve) !== 'function'
                || $.type(oDeferred.reject) !== 'function') {
                throw new DialogException("Ошибка. Не корректно передан объект ожидания!");
            }

        }

        /**
         * @memberOf BaseDialog
         * @description Проверить ширину диалогового окна
         * @private
         * @param {sap.ui.core.CSSSize} [sWidth] - ширина окна
         * @throws {execution.base.exception.DialogException} Ошибка. Не корректно передана ширина диалогового окна!
         */
        BaseDialog.prototype._checkWidth = function (sWidth) {

            if ($.type(sWidth) === 'undefined') return;

            if (sap.ui.core.CSSSize.isValid(sWidth)) return;

            throw new DialogException("Ошибка. Не корректно передана ширина диалогового окна!");

        }

        /**
         * @memberOf BaseDialog
         * @description Проверить высоту диалогового окна
         * @private
         * @param {sap.ui.core.CSSSize} [sHeight] - высота окна
         * @throws {execution.base.exception.DialogException} Ошибка. Не корректно передана высота диалогового окна!
         */
        BaseDialog.prototype._checkHeight = function (sHeight) {

            if ($.type(sHeight) === 'undefined') return;

            if (sap.ui.core.CSSSize.isValid(sHeight)) return;

            throw new DialogException("Ошибка. Не корректно передана высота диалогового окна!");

        }

        /**
         * @memberOf BaseDialog
         * @description Проверить события диалогового окна
         * @private
         * @param {array} [aEvents] - события диалогового окна
         * @throws {execution.base.exception.DialogException} Ошибка. Не корректно определены события для диалогового окна!
         */
        BaseDialog.prototype._checkEvents = function (aEvents) {

            var that = this;

            //События не переданы
            if ($.type(aEvents) === 'undefined') {

                this.setEvents(this._getDefaultEvents());
                return;
            }

            if ($.type(aEvents) !== 'array') {
                throw new DialogException("Ошибка. Не корректно определены события для диалогового окна!");
            }

            var aResults = jQuery.map(aEvents, function (oEvent, i) {

                if ($.type(oEvent.id) !== 'string') {
                    throw new DialogException("Ошибка. Не корректно определены события для диалогового окна!");
                }

                var bActive = ($.type(oEvent.active) === 'boolean') ? oEvent.active : true;

                if (!bActive) return;

                return {
                    id: oEvent.id
                }

            });

            this.setEvents(aResults);
        }

        /**
         * @memberOf BaseDialog
         * @description Проверить кнопки диалогового окна
         * @private
         * @param {array} [aButtons] - кнопки диалогового окна
         * @throws {execution.base.exception.DialogException} Ошибка. Не корректно определены кнопки для диалогового окна!
         */
        BaseDialog.prototype._checkButtons = function (aButtons) {

            var that = this;

            //Кнопки не переданы
            if ($.type(aButtons) === 'undefined') {

                this.setButtons(this._getDefaultButtons());
                return;
            }

            if ($.type(aButtons) !== 'array') {
                throw new DialogException("Ошибка. Не корректно определены кнопки для диалогового окна!");
            }

            var aResults = jQuery.map(aButtons, function (oButton, i) {

                if ($.type(oButton.id) !== 'string') {
                    throw new DialogException("Ошибка. Не корректно определены кнопки для диалогового окна!");
                }

                var bActive = ($.type(oButton.active) === 'boolean') ? oButton.active : true;

                if (!bActive) return;

                var sType = that._getButtonType(oButton.type);

                if ($.type(oButton.text) !== 'string') {
                    throw new DialogException("Ошибка. Не корректно определены кнопки для диалогового окна!");
                }

                return {
                    id: oButton.id,
                    type: sType,
                    text: oButton.text
                }

            });

            this.setButtons(aResults);
        }

        /**
         * @memberOf BaseDialog
         * @description Вернуть тип текущей кнопки
         * @private
         * @param {string} sType - тип кнопки
         * @returns {sap.m.ButtonType} Обработанный тип кнопки
         */
        BaseDialog.prototype._getButtonType = function (sType) {

            switch (sType) {
                case sap.m.ButtonType.Accept:
                    return sap.m.ButtonType.Accept;
                case sap.m.ButtonType.Reject:
                    return sap.m.ButtonType.Reject;
                case sap.m.ButtonType.Emphasized:
                    return sap.m.ButtonType.Emphasized;
                default:
                    return sap.m.ButtonType.Default;
            }
        }

        /**
         * @memberOf BaseDialog
         * @description Вернуть кнопки по умолчанию для диалогового окна
         * @private
         * @returns {array} Массив кнопок по умолчанию
         */
        BaseDialog.prototype._getDefaultButtons = function () {
            return [{
                id: BTN_CLOSE_ID,
                text: Utils.getBundleText("BASE_BTN_CLOSE"),
                type: sap.m.ButtonType.Default
            }]
        }

        /**
         * @memberOf BaseDialog
         * @description Вернуть события по умолчанию для диалогового окна
         * @private
         * @returns {array} Массив событий по умолчанию
         */
        BaseDialog.prototype._getDefaultEvents = function () {
            return [{
                id: DLG_EVENT_AFTER_CLOSE
            }];
        }

        return BaseDialog;
    });