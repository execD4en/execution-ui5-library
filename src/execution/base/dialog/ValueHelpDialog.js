sap.ui.define(["execution/base/dialog/BaseDialog",
               "execution/base/exception/BaseException",
               "sap/ui/model/json/JSONModel",
               "sap/ui/base/DataType",
               "execution/base/util/Utils",
               "sap/ui/table/Table",
               "sap/ui/model/FilterOperator"],
               
	function(BaseDialog,
			 BaseException,
			 JSONModel,
			 DataType,
			 Utils,
			 Table,
			 FilterOperator){
	
		var INITIAL_SELECTION_MODE = "None",
		    INITIAL_SELECTION_BEHAVIOUR = "Row",
		    INITIAL_ROW_HEIGHT = 38,
		    INITIAL_COLUMN_HEADER_HEIGHT = 38,
		    INITIAL_COLUMN_HEADER_VISIBLE = true,
		    INITIAL_SHOW_NO_DATA = true,
		    TABLE_EVENT_ROW_SELECT = "row_select",
		    INITIAL_SHOW_SEARCHFIELD = true,
		    OTH_EVENT_SEARCH = "search";
	
		return BaseDialog.extend("execution.base.dialog.ValueHelpDialog",{
			
			constructor: function(mParam) {
				
				this._tableEvents = null;
				this._otherEvents = null;
				
				this._localModel = new JSONModel();
				
				BaseDialog.prototype.constructor.apply(this, arguments);
				
				this._table = null;
				
				this._searchField = null;
				this._showSearchField = ($.type(mParam.showSearchField) === 'boolean') ? mParam.showSearchField : INITIAL_SHOW_SEARCHFIELD;
					
				this._selectionMode = mParam.table.selectionMode || INITIAL_SELECTION_MODE;
				this._selectionBehaviour = mParam.table.selectionBehaviour || INITIAL_SELECTION_BEHAVIOUR; 
				this._rowHeight = mParam.table.rowHeight || INITIAL_ROW_HEIGHT;
				this._columnHeaderHeight = mParam.table.columnHeaderHeight || INITIAL_COLUMN_HEADER_HEIGHT;
				this._columnHeaderVisible = ($.type(mParam.table.columnHeaderVisible) === 'boolean') ? mParam.table.columnHeaderVisible : INITIAL_COLUMN_HEADER_VISIBLE;
				this._showNoData = ($.type(mParam.table.showNoData) === 'boolean') ? mParam.table.showNoData : INITIAL_SHOW_NO_DATA;
				this._noDataText = mParam.table.noDataText || Utils.getBundleText("BASE_NO_DATA_TEXT");
			},
			
			onSearchData: function(oEvent) {
				
				var oBinding = this._getDataBinding();
				var oSearchEvent = this._getOtherEvent(OTH_EVENT_SEARCH);
				
				if (!(oBinding instanceof sap.ui.model.Binding)) return;
				if ($.type(oSearchEvent) !== 'object') return;
				
				var sQuery = oEvent.getParameter("newValue");
				
				if (sQuery === ""){
					oBinding.filter([]);
					return;
				}
				
				var aResults = jQuery.map(oSearchEvent.filters, function(oFilter, index){
					return new sap.ui.model.Filter(oFilter.fieldId, oFilter.operator, sQuery);
				});
				
				var oFilter = new sap.ui.model.Filter({
					and : oSearchEvent.and,
					filters: aResults
				});
				
				oBinding.filter(oFilter);
			},
			
			onRowSelect: function(oEvent) {
				
				var oTable = this.getTable();
				var oDeferred = this.getDeferred();
				var oDialog = this.getDialog();
				
				var aIndices = oEvent.getParameter("rowIndices");
				
				var aData = jQuery.map(aIndices, function(iIndex, i){
					var oCtx = oTable.getContextByIndex(iIndex);
					
					if (!(oCtx instanceof sap.ui.model.Context)) return;
					
					return oCtx.getObject();
				});
				
				if (aData.length === 0) return;
				
				if ($.type(oDeferred) !== 'null') oDeferred.resolve();
				
				oDialog.close();
				
			},
			
			destroy: function() {
				
				if ($.type(this.getSearchField()) !== 'null') this.getSearchField().destroy();
				
				this.getLocalModel()
					.destroy();
				
				this._localModel = null;
				
				this.getTable()
					.destroy();
				
				this._table = null;
				
				this._tableEvents = null;
				this._otherEvents = null;
				
				BaseDialog.prototype.destroy.apply(this, arguments);
				
			},
			
			build: function() {
				
				BaseDialog.prototype.build.apply(this, arguments);
				
				var oDialog = this.getDialog();
				
				if (this.getShowSearchField()) {
					
					this._searchField = new sap.m.SearchField({
						liveChange: this.onSearchData.bind(this),
						width: "50%"
					}).addStyleClass("execBaseDlgSearch");
					
				}
				
				oDialog.addContent(this._searchField);
				
				this._table = new Table({
					selectionMode: this.getSelectionMode(),
					selectionBehaviour: this.getSelectionBehaviour(),
					rowHeight: this.getRowHeight(),
					columnHeaderHeight: this.getColumnHeaderHeight(),
					columnHeaderVisible: this.getColumnHeaderVisible(),
					showNoData: this.getShowNoData(),
					noData: this.getNoDataText()
				});
				
				this._attachTableEvents();
				
				oDialog.addContent(this._table);
				
				return oDialog;
			},
			
			getSelectionMode: function() {
				return this._selectionMode;
			},
			
			getSelectionBehaviour: function() {
				return this._selectionBehaviour;
			},
			
			getRowHeight: function() {
				return this._rowHeight;
			},
			
			getColumnHeaderHeight: function() { 
				return this._columnHeaderHeight;
			},
			
			getColumnHeaderVisible: function() {
				return this._columnHeaderVisible;
			},
			
			getShowNoData: function(){
				return this._showNoData;
			},
			
			getNoDataText: function() {
				return this._noDataText;
			},
			
			setOtherEvents: function(aData) {
				this._otherEvents = aData;
			},
			
			getOtherEvents: function() {
				return this._otherEvents;
			},
			
			getTableEvents: function() {
				return this._tableEvents;
			},
			
			setTableEvents: function(aData) {
				this._tableEvents = aData;
			},
			
			setData: function(aData) {
				this.getLocalModel()
				    .setProperty("/table/data", aData);
			}, 
			
			getData: function() {
				return this.getLocalModel()
						   .getProperty("/table/data");
			},
			
			setColumns: function(aData) {
				this.getLocalModel()
			    	.setProperty("/table/columns", aData);
			},
			
			getColumns: function() {
				return this.getLocalModel()
				   		   .getProperty("/table/columns");
			},
			
			getTable: function() {
				return this._table;
			},
			
			getLocalModel: function() {
				return this._localModel;
			},
			
			getShowSearchField: function() {
				return this._showSearchField;
			},
			
			getSearchField: function() {
				return this._searchField;
			},
			
			_getDataBinding: function() {
				return this.getTable()
				           .getBinding("rows");
			},
			
			_attachTableEvents: function() {
				
				var that = this;
				
				var oTable = this.getTable();
				var aTableEvents = this.getTableEvents();
				
				if (aTableEvents.length === 0) return;
				
				jQuery.each(aTableEvents, function(i, oEvent){
					
					switch(oEvent.id) {
					case TABLE_EVENT_ROW_SELECT:
						oTable.attachRowSelectionChange(that.onRowSelect, that);
						break;
					}
					
				});
				
			},
			
			_getDefaultTableEvents: function() {
				return [];
			},
			
			_getOtherEvent: function(sId) {
				
				var aEvents = this.getOtherEvents();
				
				return Utils.getDataByPropertyKeyInArray(aEvents, "id", sId);
			},
			
			_getDefaultOtherEvents: function() {
				return [];
			},
			
			_checkParam: function(mParam) {
				
				this._checkTableEvents(mParam.tableEvents);
				this._checkOtherEvents(mParam.otherEvents);
				this._checkTableParam(mParam.table);
				
			},
			
			_checkSearchEvent: function(oEvent) {
				
				if ($.type(oEvent.filters) !== 'array'
						|| oEvent.filters.length === 0){
					throw new BaseException("Ошибка. Не корректно передано событие поиска данных в таблице!");
				}	
				
				if ($.type(oEvent.and) !== 'boolean'){
					throw new BaseException("Ошибка. Не корректно передано событие поиска данных в таблице!");
				}
				
				jQuery.each(oEvent.filters, function(i, oFilter) {
					
					if (!DataType.getType("string")
								 .isValid(oFilter.fieldId)){
						
						throw new BaseException("Ошибка. Не корректно передано событие поиска данных в таблице!");
					}
					
					if (!DataType.getType("string")
							 	 .isValid(oFilter.operator)){
						throw new BaseException("Ошибка. Не корректно передано событие поиска данных в таблице!");
					}
					
					if (!(oFilter.operator in FilterOperator)) {
						throw new BaseException("Ошибка. Не корректно передано событие поиска данных в таблице!");
					}
					
				});
				
			},
			
			_checkOtherEvents: function(aEvents) {
				
				var that = this;
				
				if ($.type(aEvents) === 'undefined'){
					this.setTableEvents(this._getDefaultOtherEvents());
					return;
				}
				
				if ($.type(aEvents) !== 'array'){
					throw new BaseException("Ошибка. Не корректно переданы общие события!");
				}
				
				var aResults = jQuery.map(aEvents, function(oEvent, i){
					
					if ($.type(oEvent.id) !== 'string'){
						throw new BaseException("Ошибка. Не корректно переданы общие события!");
					}
					
					var bActive = ($.type(oEvent.active) === 'boolean') ? oEvent.active : true;
					
					if (!bActive) return;
					
					switch(oEvent.id){
					case OTH_EVENT_SEARCH: 
						that._checkSearchEvent(oEvent);
						break;
					}
					
					return {
						id: oEvent.id
					}
					
				});
				
				this.setOtherEvents(aResults);
			},
			
			_checkTableEvents: function(aEvents) {
				
				if ($.type(aEvents) === 'undefined'){
					this.setTableEvents(this._getDefaultTableEvents());
					return;
				}
				
				if ($.type(aEvents) !== 'array'){
					throw new BaseException("Ошибка. Не корректно переданы события для таблицы!");
				}
				
				var aResults = jQuery.map(aEvents, function(oEvent, i){
					
					if ($.type(oEvent.id) !== 'string'){
						throw new BaseException("Ошибка. Не корректно определены события для таблицы!");
					}
					
					var bActive = ($.type(oEvent.active) === 'boolean') ? oEvent.active : true;
					
					if (!bActive) return;
					
					return {
						id: oEvent.id
					}
					
				});
				
				this.setTableEvents(aResults);
			},
			_checkData: function(aData) {
				if ($.type(aData) !== 'array'
					|| aData.length === 0){
					throw new BaseException("Ошибка. Не переданы данные строк для таблицы!");
				}
				
				this.setData(aData);
			},
			_checkColumns: function(aColumns) {
				if ($.type(aColumns) !== 'array'
					|| aColumns.length === 0) {
					throw new BaseException("Ошибка. Не переданы данные колонок для таблицы!");
				}
				
				this.setColumns(aColumns);
			},

			_checkSelectionMode: function(sMode) {
				
				if ($.type(sMode) === 'undefined') return;
				
				if ($.type(sMode) !== 'string'){
					throw new BaseException("Ошибка. Не корректно передан тип выбора строк таблицы!");
				}
				
				switch(sMode) {
				case sap.ui.table.SelectionMode.MultiToggle: return;
				case sap.ui.table.SelectionMode.None: return;
				case sap.ui.table.SelectionMode.Single: return;
				default:
					
					throw new BaseException("Ошибка. Не корректно передан тип выбора строк таблицы!");
				
				}
				
			},
			
			_checkSelectionBehaviour: function(sBehaviour) {
				
				if ($.type(sBehaviour) === 'undefined') return;
				
				if ($.type(sBehaviour) !== 'string'){
					throw new BaseException("Ошибка. Не корректно передан параметр поведения таблицы при выборе строк!");
				}
				
				switch(sBehaviour) {
				case sap.ui.table.SelectionBehavior.Row: return;
				case sap.ui.table.SelectionBehavior.RowOnly: return;
				case sap.ui.table.SelectionBehavior.RowSelector: return;
				default:
					
					throw new BaseException("Ошибка. Не корректно передан параметр поведения таблицы при выборе строк");
				
				}
				
			},
			
			_checkRowHeight: function(iHeight){
				
				if ($.type(iHeight) === 'undefined') return;
				
				if (DataType.getType("int").isValid(iHeight)) return;
				
				throw new BaseException("Ошибка. Не корректно передан параметр размера строки таблицы!");
			},
			
			_checkColumnHeaderHeight: function(iHeight){
				
				if ($.type(iHeight) === 'undefined') return;
				
				if (DataType.getType("int").isValid(iHeight)) return;
				
				throw new BaseException("Ошибка. Не корректно передан параметр размера блока с колонками таблицы!");
			},
			
			_checkNoDataText: function(sText) {
				if ($.type(sText) === 'undefined') return;
				
				if (DataType.getType("string").isValid(sText)) return;
				
				throw new BaseException("Ошибка. Не корректно передано сообщение для отображения отсутствия строк таблицы!");
			
			},
			
			_checkTableParam: function(mParam) {
				
				this._checkData(mParam.data);
				this._checkColumns(mParam.columns);
				this._checkSelectionMode(mParam.selectionMode);
				this._checkSelectionBehaviour(mParam.selectionBehaviour);
				this._checkRowHeight(mParam.rowHeight);
				this._checkColumnHeaderHeight(mParam.columnHeaderHeight);
				this._checkNoDataText(mParam.noDataText);
				
			}
			
			
			
		});

}) 