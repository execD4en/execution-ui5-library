sap.ui.define(["execution/base/dialog/BaseDialog",
               "execution/base/exception/BaseException"], 
	function(BaseDialog,
			 BaseException){
	
		return BaseDialog.extend("execution.base.dialog.MessageDialog",{
			constructor: function(mParam) {
				
				BaseDialog.prototype.constructor.apply(this, arguments);
				
				this._message = ($.type(mParam.message) === 'array') ? mParam.message.join("\n") : mParam.message;
				this._state = ($.type(mParam.state) === 'undefined') ? sap.ui.core.ValueState.Error: mParam.state;
				
			},
			destroy: function(){
				
				var anyMessage = this.getMessage();
				
				anyMessage = null;
				
				BaseDialog.prototype.destroy.apply(this,arguments);
			},
			getState: function() {
				return this._state;
			},
			getMessage: function() {
				return this._message;
			},
			build: function(){
				
				BaseDialog.prototype.build.apply(this, arguments);
				
				var oDialog = this.getDialog();
				
				oDialog.setState(this.getState());
				
				var oText = new sap.m.Text({
					text: this.getMessage()
				}).addStyleClass("sapUiMediumMargin");
				
				oDialog.addContent(oText);
				
				return oDialog;
			},
			_checkParam: function(mParam){
				
				BaseDialog.prototype._checkParam.apply(this, arguments);
				
				this._checkMessage(mParam.message);
				this._checkState(mParam.state);
				
			},
			_checkState: function(sState){
				
				if ($.type(sState) === 'undefined') return;
				
				if ($.type(sState) !== 'string'){
					throw new BaseException("Ошибка. Не корректно передано состояние диалогового окна!");
				}
				
				switch(sState){
				case sap.ui.core.ValueState.Success: return;
				case sap.ui.core.ValueState.None: return;
				case sap.ui.core.ValueState.Warning: return;
				case sap.ui.core.ValueState.Error: return;
				default:
					throw new BaseException("Ошибка. Не корректно передано состояние диалогового окна!");
				}
			},
			_checkMessage: function(anyMessage){
				
				if ($.type(anyMessage) !== 'array'
					&& $.type(anyMessage) !== 'string'){
					throw new BaseException("Ошибка. Не переданы сообщения для вывода в диалоговом окне!");
				}
			
				if ($.type(anyMessage) === 'array') {
					
					if (anyMessage.length === 0){
						throw new BaseException("Ошибка. Не переданы сообщения для вывода в диалоговом окне!");
					}
				
					jQuery.each(anyMessage, function(index, sMsg){
					
						if ($.type(sMsg) !== 'string') {
							throw new BaseException("Ошибка. Не корректно переданы сообщения!");
						}
					
					});
					
				}
			}
		});
});