sap.ui.define(["execution/base/dialog/MessageDialog",
               "execution/base/util/Utils"],
	function(MessageDialog,
			 Utils){
	
		var BTN_ACCEPT_ID = "accept", 
			BTN_REJECT_ID = "reject";
	
		return MessageDialog.extend("execution.base.dialog.ConfirmDialog",{
			onAccept: function() {
				var oDialog = this.getDialog();
				var oDeferred = this.getDeferred();
				
				if ($.type(oDeferred) !== 'null') oDeferred.resolve();
				
				oDialog.close();
			},
			onReject: function() {
				var oDialog = this.getDialog();
				var oDeferred = this.getDeferred();
				
				if ($.type(oDeferred) !== 'null') oDeferred.reject();
				
				oDialog.close();
			},
			_getDefaultButtons: function() {
				return [{
					id : BTN_ACCEPT_ID,
					text: Utils.getBundleText("BASE_BTN_ACCEPT"),
					type : sap.m.ButtonType.Accept
				},
				{
					id : BTN_REJECT_ID,
					text: Utils.getBundleText("BASE_BTN_REJECT"),
					type : sap.m.ButtonType.Reject
				}]
			},
			_applyButtons: function() {
				
				var that = this;
				
				var aButtons = this.getButtons();
				var oDialog = this.getDialog();
				var aDefaultButtons = this._getDefaultButtons();
				
				var sAcceptText = Utils.getBundleText("BASE_BTN_ACCEPT");
				var sRejectText = Utils.getBundleText("BASE_BTN_REJECT");
				
				var sAcceptType = sap.m.ButtonType.Accept;
				var sRejectType = sap.m.ButtonType.Reject;
				
				jQuery.each(aButtons, function(i, oButton){
					
					switch(oButton.id) {
					case BTN_ACCEPT_ID: 
						sAcceptText = oButton.text;
						sAcceptType = oButton.type;
						break;
					case BTN_REJECT_ID:
						sRejectText = oButton.text;
						sRejectType = oButton.type;
						break;
					}
					
				});
				
				jQuery.each(aDefaultButtons, function(i, oButton){
					
					var oElement = null;
					
					switch(oButton.id) {
					case BTN_ACCEPT_ID:
						
						oElement = new sap.m.Button({
							text: sAcceptText,
							type: sAcceptType,
							press: that.onAccept.bind(that)
						});
						break;
						
					case BTN_REJECT_ID:
						
						oElement = new sap.m.Button({
							text: sRejectText,
							type: sRejectType,
							press: that.onReject.bind(that)
						});
						break;
						
					default: return true;
					}
					
					oDialog.addButton(oElement);
				});
				
				
			}
		});

})