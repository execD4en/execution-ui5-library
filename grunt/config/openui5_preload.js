module.exports = function (grunt, config) {
    var libraryNamespace = config.library.name.replace(/\./g, '/');

    var path = config.library.main.path;
    var dest = config.library.main.target;
    var version = config.library.version;
    var name = config.library.name;

    var openui5_preload = {
        options: {
            compatVersion: '1.52'
        }
    };

    openui5_preload["library-" + name] = {
        options: {
            resources: path,
            dest: dest
        },
        libraries: libraryNamespace
    };

    return openui5_preload;
};