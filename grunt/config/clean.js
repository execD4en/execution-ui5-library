module.exports = function (grunt, config) {

    var clean = {
        'surefire-reports': {
            src: [config.library.test.reports]
        },
        'library': {
            src: [config.targetPath]
        },
        'docs': {
            src: [config.library.docs.path]
        }
    }

    return clean;
};