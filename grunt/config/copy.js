// copy files/folders
module.exports = function (grunt, config) {

    var libraryNamespace = config.library.name.replace(/\./g, '/');

    var copy = {};

    copy['test'] = {
        expand: true,
        cwd: config.library.test.path,
        src: ['**'],
        dest: config.library.test.target
    };

    copy['main'] = {
        files: [
            {
                expand: true,
                src: ["**", "!**/manifest.json"],
                cwd: config.library.main.path,
                dest: config.library.main.target
            },
            {
                expand: true,
                src: ["**/manifest.json"],
                cwd: config.library.main.path,
                flatten: true,
                dest: config.targetPath
            }
        ]
    };

    return copy;
};