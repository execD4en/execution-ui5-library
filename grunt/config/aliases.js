module.exports = function (grunt, config) {

    return {
        'upload': function () {
            var sMode = config.mode;

            if (sMode !== 'prd') return;

            grunt.task.run('nwabap_ui5uploader');
        },
        // Build task
        'build': function () {
            grunt.task.run('openui5_preload:library-' + config.library.name);
            grunt.task.run('openui5_theme');
        },

        'test': function (mode) {

            if (!mode || (mode !== 'src' && mode !== 'target')) {
                mode = 'src';
            }

            // listen to the connect server startup
            grunt.event.on('connect.*.listening', function (hostname, port) {

                // 0.0.0.0 does not work in windows
                if (hostname === '0.0.0.0') {
                    hostname = 'localhost';
                }

                grunt.config(['selenium_qunit', 'options', 'baseUrl'], 'http://' + hostname + ':' + port);

                grunt.task.run(['selenium_qunit:run']);
            });

            // cleanup and start connect server
            grunt.task.run([
                'openui5_connect:' + mode// + ":keepalive"
            ]);
        },


        // Default task (called when just running "grunt")
        'default':
            [
                'clean',
                'test',
                'build',
                'copy',
                'jsdoc',
                'upload'
            ]

    };
}
;