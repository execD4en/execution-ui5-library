module.exports = function(grunt, config) {

    if (!grunt.option('port')) {
        grunt.option('port', 8080);
    }

    if (typeof grunt.option('hostname') !== 'string') {
        grunt.option('hostname', '*');
    }

    var connect  = {
        options: {
            // set default port
            port: +grunt.option('port'),
            // use the next best port if specified port is already in use
            useAvailablePort: true,
            hostname: '<%= grunt.option("hostname") %>'

        },
        src: {},
        target: {}
    }


    return connect;
}