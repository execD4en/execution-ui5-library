module.exports = function (grunt, config) {

    var nwabap_ui5uploader = {
        options: {
            conn: {
                server: config.server.uri,
            },
            auth: {
                user: config.server.auth.user,
                pwd: config.server.auth.password
            }
        },
        upload_build: {
            options: {
                ui5: {
                    package: config.server.repository.package,
                    bspcontainer: config.server.repository.bspApp,
                    bspcontainer_text: config.server.repository.bspText,
                    transportno: config.server.repository.transport
                },
                resources: {
                    cwd: config.targetPath,
                    src: '**/*.*'
                }
            }
        }
    };

    return nwabap_ui5uploader;
}