module.exports = function (grunt, config) {

    var jsdoc = {
        dist: {
            src: [config.library.main.path + '/**/*.js'],
            options: {
                private: true,
                destination: config.library.docs.path,
                template: config.library.docs.template,
                configure: config.library.docs.config
            }
        }
    }


    return jsdoc;
}