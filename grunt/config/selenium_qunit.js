module.exports = function(grunt, config) {

    var browsers = grunt.option('browsers');
    if (!browsers) {
        browsers = [ 'chrome' ]; // default
    } else {
        browsers = browsers.split(',');
    }

    return {
        run: {
            options: {
                browsers: browsers,
                contextPath: '',
                reportsDir: config.library.test.reports,
                testPages: [ config.library.test.testsuite ]
            }
        }
    };
};