module.exports = function (grunt, config) {

    var openui5_connect = {

        options: {
            contextpath: '/',
            proxypath: 'proxy',
            proxyOptions: {
                secure: false
            },
            cors: {
                origin: "*"
            }

        },

        src: {

            options: {
                resources: [
                    config.library.main.path,
                    'bower_components/openui5-sap.ui.core/resources',
                    'bower_components/openui5-sap.m/resources',
                    'bower_components/openui5-sap.ui.table/resources',
                    'bower_components/openui5-sap.ui.unified/resources',
                    'bower_components/openui5-themelib_sap_belize/resources'
                ],
                testresources: [
                    config.library.test.path,
                    'bower_components/openui5-sap.ui.core/test-resources',
                    'bower_components/openui5-sap.m/test-resources',
                    'bower_components/openui5-sap.ui.table/test-resources',
                    'bower_components/openui5-sap.ui.unified/test-resources',
                    'bower_components/openui5-themelib_sap_belize/test-resources'
                ]

            }

        },

        target: {

            options: {

                resources: ['target/resources/execution/base'],

                testresources: ['target/test-resources/execution/base']

            }

        }

    };

    return openui5_connect;
};