module.exports = function (grunt, config) {

    var openui5_theme = {
        options: {
            rootPaths: [
                'bower_components/openui5-sap.ui.core/resources',
                config.library.main.path
            ],
            library: {
                name: config.library.name
            }
        },
        library: {
            files: [
                {
                    expand: true,
                    cwd: config.library.main.path,
                    src: '**/themes/*/library.source.less',
                    dest: config.library.main.target
                }
            ]
        }

    }

    return openui5_theme;
}