sap.ui.define([
    "execution/sample/MTableConfig/controller/BaseController",
    "sap/ui/model/json/JSONModel",
    "execution/base/table/m/TableConfig"
], function(BaseController,
            JSONModel,
            TableConfig) {
    "use strict";

    return BaseController.extend("execution.sample.MTableConfig.controller.Home", {
        onInit: function() {

            var oModel = new JSONModel({
                data: [
                    {
                        test: "123",
                        test2: "test"
                    },
                    {
                        test: "Тест",
                        test2: "123"
                    }
                ],
                columns: [
                    {
                        field: "test",
                        type: "text",
                        label: "Тестовая колонка",
                        width: "50%"
                    },
                    {
                        field: "test2",
                        type: "text",
                        label: "Вторая тестовая колонка",
                        width: "50%"
                    }
                ]
            });

            this._tconfig = new TableConfig({
                column:{
                    model: oModel,
                    path:'/columns'
                },
                row:{
                    model:oModel,
                    path:"/data"
                }
            });

            this._tconfig.build(this.byId('idTable'));

        }
    });

});