sap.ui.define([
    "execution/base/dialog/BaseDialog"
], function (
    BaseDialog
) {

    var page = new sap.m.Page("myFirstPage", {
        title: "Dialog Test",
        showNavButton: true,
        enableScrolling: true
    });

    var app = new sap.m.App("myApp", {
        initialPage: "myFirstPage"
    });

    app.addPage(page).placeAt("qunit-fixture");

    function createInitialDialog() {
        var oBaseDialog = null;

        try {
            oBaseDialog = new BaseDialog();
        }
        catch (oError) {
        }

        return oBaseDialog;
    }

    function createComplexDialog(mParam) {
        var oBaseDialog = null;

        var sMessage = null;
        var sType = null;

        try {
            oBaseDialog = new BaseDialog(mParam);
        }
        catch (oError) {
            sMessage = oError.message;
            sType = oError.name;
        }

        return {
            dialog : oBaseDialog,
            message: sMessage,
            type : sType
        };
    }

    QUnit.module("Initial Check");

    QUnit.test("Initialization", function (assert) {

        var oBaseDialog = createInitialDialog();

        assert.ok($.type(oBaseDialog) !== 'null' && oBaseDialog instanceof BaseDialog, "Creation of the initial BaseDialog is ok!");

    });

    QUnit.test("Build Dialog", function (assert) {
        var oBaseDialog = createInitialDialog();

        var oDialog = oBaseDialog.build();

        assert.ok(oDialog instanceof sap.m.Dialog, "Dialog instance is correct");
    });

    QUnit.test("Open/Close Dialog", function (assert) {
        assert.expect( 2 );

        var done = assert.async();

        var oBaseDialog = createInitialDialog();

        var oDialog = oBaseDialog.build();

        page.addContent(oDialog);

        oDialog.open();

        assert.ok(oDialog.isOpen(), "Dialog is opened correctly!");

        oDialog.attachAfterClose(function(){
            assert.ok(!oDialog.isOpen(), "Dialog is closed correctly!");
            done();
        });

        oDialog.close();
        page.removeAllContent();
    });

    QUnit.test("Check after close event",function (assert) {
        assert.expect( 2 );

        var done = assert.async();

        var oBaseDialog = createInitialDialog();

        var oDialog = oBaseDialog.build();

        page.addContent(oDialog);

        oDialog.open();

        oDialog.attachAfterClose(function(){
            assert.ok($.type(oBaseDialog.getDialog()) === 'null', "Dialog Box instance is destroyed correctly!");
            assert.ok($.type(oBaseDialog.getEvents()) && $.type(oBaseDialog.getButtons()) === 'null', "Buttons and events are nulled!");
            done();
        });

        oDialog.close();
        page.removeAllContent();
    });

    QUnit.module("Custom parameters");

    QUnit.test("Width/height check", function (assert) {

        var oSettings = createComplexDialog({
            width: "фывфывыфв",
            height: "100%"
        });

        var sMessage = oSettings.message;
        var sType = oSettings.type;

        assert.ok($.type(oSettings.dialog) === 'null' && sMessage && sType === 'DialogError', sMessage || "Обработка ширины/высоты не корректна!");

        var oSettings = createComplexDialog({
            width: "100px",
            height: "10asdasdsad"
        });

        var sMessage = oSettings.message;
        var sType = oSettings.type;

        assert.ok($.type(oSettings.dialog) === 'null' && sMessage && sType === 'DialogError', sMessage || "Обработка ширины/высоты не корректна!");

        var oSettings = createComplexDialog({
            width: null,
            height: "100%"
        });

        var sMessage = oSettings.message;
        var sType = oSettings.type;

        assert.ok($.type(oSettings.dialog) === 'null' && sMessage && sType === 'DialogError', sMessage || "Обработка ширины/высоты не корректна!");

        var oSettings = createComplexDialog({
            width: "100%",
            height: null
        });

        var sMessage = oSettings.message;
        var sType = oSettings.type;

        assert.ok($.type(oSettings.dialog) === 'null' && sMessage && sType === 'DialogError', sMessage || "Обработка ширины/высоты не корректна!");

        var oSettings = createComplexDialog({
            width: 123,
            height: "100%"
        });

        var sMessage = oSettings.message;
        var sType = oSettings.type;

        assert.ok($.type(oSettings.dialog) === 'null' && sMessage && sType === 'DialogError', sMessage || "Обработка ширины/высоты не корректна!");

        var oSettings = createComplexDialog({
            width: "250px",
            height: "250px"
        });

        var sMessage = oSettings.message;
        var sType = oSettings.type;

        assert.ok($.type(oSettings.dialog) !== 'null' && oSettings.dialog instanceof BaseDialog && !sMessage && !sType, "Ширина и высота заданы корректно!");

        var oSettings = createComplexDialog({
            width: "auto",
            height: "auto"
        });

        var sMessage = oSettings.message;
        var sType = oSettings.type;

        assert.ok($.type(oSettings.dialog) !== 'null' && oSettings.dialog instanceof BaseDialog && !sMessage && !sType, "Ширина и высота заданы корректно!");
    });



});